package com.example.misael.misaeljonathanprofile;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class TransisiWelcome2 extends AppCompatActivity {
    private static int WELCOME_TIMEOUT = 4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_speech2);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent welcome = new Intent(TransisiWelcome2.this, SecondClass.class);
                startActivity(welcome);
                overridePendingTransition(R.anim.fadeinutama, R.anim.fadeout);

                finish();

            }
        }, WELCOME_TIMEOUT);
    }
}