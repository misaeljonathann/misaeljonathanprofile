package com.example.misael.misaeljonathanprofile;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;


public class SecondClass extends AppCompatActivity {
    private ImageView mScanner;
    private Animation mAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mScanner = (ImageView)findViewById(R.id.unlockMe_click);

        mAnimation = new TranslateAnimation(0, 0, 0, 100);
        mAnimation.setDuration(1000);
//        mAnimation.setFillAfter(true);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mScanner.setAnimation(mAnimation);
        mScanner.setVisibility(View.VISIBLE);
        
        ImageView unlockMe = (ImageView) findViewById(R.id.unlockMe_click);
        unlockMe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondClass.this, TheProfile.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        });
    }
}