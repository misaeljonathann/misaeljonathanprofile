package com.example.misael.misaeljonathanprofile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.jgabrielfreitas.core.BlurImageView;

/**
 * Created by Misael on 4/4/2018.
 */

public class TheExperience extends AppCompatActivity {
    BlurImageView blurImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.experience_main);

        blurImage = (BlurImageView) findViewById(R.id.background_imageview);
        blurImage.setBlur(2);

        TextView prof_btn=(TextView) findViewById(R.id.profile_tv);
        TextView exp_btn = (TextView) findViewById(R.id.experience_tv);
        TextView con_btn = (TextView) findViewById(R.id.connect_tv);

        prof_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheExperience.this, TheProfile.class);
                startActivity(intent);
            }
        });

        exp_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheExperience.this, TheExperience.class);
                startActivity(intent);
            }
        });

        con_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TheExperience.this, TheContact.class);
                startActivity(intent);
            }
        });
    }

//    public void buttonClickFunction(View v) {
//        Intent intent = null;
//        switch(v.getId()) {
//            case R.id.profile_tv: // R.id.textView1
//                intent = new Intent(this, TheProfile.class);
//                break;
//            case R.id.experience_tv: // R.id.textView2
//                intent = new Intent(this, TheExperience.class);
//                break;
//            case R.id.connect_tv: // R.id.textView3
//                intent = new Intent(this, TheContact.class);
//        }
//        startActivity(intent);
//    }
}