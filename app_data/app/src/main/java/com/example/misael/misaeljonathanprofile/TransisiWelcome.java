package com.example.misael.misaeljonathanprofile;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class TransisiWelcome extends AppCompatActivity {
    private static int WELCOME_TIMEOUT = 4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_speech);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent welcome = new Intent(TransisiWelcome.this, TransisiWelcome2.class);
                startActivity(welcome);
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);

                finish();

            }
        }, WELCOME_TIMEOUT);
    }
}
